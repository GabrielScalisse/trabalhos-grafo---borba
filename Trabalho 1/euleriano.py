from copy import copy

'''
	is_connected - Verifica se um gráfico na forma de um dicionário é
conectado ou não, usando o algoritmo de pesquisa de largura de canal (BFS)
'''
def is_connected(G):
	inicia_nos = list(G)[0]
	cor = {v: 'white' for v in G}
	cor[inicia_nos] = 'gray'
	S = [inicia_nos]
	while len(S) != 0:
		u = S.pop()
		for v in G[u]:
			if cor[v] == 'white':
				cor[v] = 'gray'
				S.append(v)
			cor[u] = 'black'
	return list(cor.values()).count('black') == len(G)

'''
	odd_degree_nodes - retorna uma lista de todos os nós G graus ímpares
'''
def odd_degree_nodes(G):
	odd_degree_nodes = []
	for u in G:
		if len(G[u]) % 2 != 0:
			odd_degree_nodes.append(u)
	return odd_degree_nodes

'''
	from_dict - retornar uma lista de ligacoes de tuplas de um grafo G em um
formato de dicionário
'''	
def from_dict(G):
	ligacoes = []
	for u in G:
		for v in G[u]:
			ligacoes.append((u,v))
	return ligacoes

'''
	fleury(G) - retornar trilha euleriana do gráfico G ou um
string 'Não Gráfico Euleriano' se não for possível traçar um caminho
'''
def fleury(G):
	'''
		verifica se G tem ciclo euleriano ou trilha
	'''
	odn = odd_degree_nodes(G)
	if len(odn) > 2 or len(odn) == 1:
		return 'Gráfico não euleriano'
	else:
		g = copy(G)
		trilha = []
		if len(odn) == 2:
			u = odn[0]
		else:
			u = list(g)[0]
		while len(from_dict(g)) > 0:
			vertice_atual = u
			for u in g[vertice_atual]:
				g[vertice_atual].remove(u)
				g[u].remove(vertice_atual)
				ponte = not is_connected(g)
				if ponte:
					g[vertice_atual].append(u)
					g[u].append(vertice_atual)
				else:
					break
			if ponte:
				g[vertice_atual].remove(u)
				g[u].remove(vertice_atual)
				g.pop(vertice_atual)
			trilha.append((vertice_atual, u))
	return trilha

# testando sete pontes de konigsberg
print('Konigsberg')
G = {0: [2, 2, 3], 1: [2, 2, 3], 2: [0, 0, 1, 1, 3], 3: [0, 1, 2]}
print(fleury(G))

# testando um ciclo euleriano
print('Primeiro Ciclo euleriano')
G = {0: [1, 4, 6, 8], 1: [0, 2, 3, 8], 2: [1, 3], 3: [1, 2, 4, 5], 4: [0, 3], 5: [3, 6], 6: [0, 5, 7, 8], 7: [6, 8], 8: [0, 1, 6, 7]}
print(fleury(G))

# testando outro ciclo euleriano
print('Segundo Ciclo euleriano')
G = {1: [2, 3, 4, 4], 2: [1, 3, 3, 4], 3: [1, 2, 2, 4], 4: [1, 1, 2, 3]}
print(fleury(G))

# testando trilha euleriana
print('Trilha euleriana')
G = {1: [2, 3], 2: [1, 3, 4], 3: [1, 2, 4], 4: [2, 3]}
print(fleury(G))