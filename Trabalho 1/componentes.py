
class Dado(object):
    def __init__(self, nome):
        self.__nome  = nome
        self.__ligacoes = set()

    @property
    def nome(self):
        return self.__nome

    @property
    def ligacoes(self):
        return set(self.__ligacoes)

    def add_ligacao(self, outro):
        self.__ligacoes.add(outro)
        outro.__ligacoes.add(self)

# A função para procurar componentes conectados.
def conectando_componentes(nos):

    # Lista de componentes conectados encontrados. A ordem é aleatória.
    resultado = []

    # Faz uma cópia do conjunto, para que possamos modificá-lo.
    nos = set(nos)

    # Repete enquanto ainda temos nós para processar.
    while nos:

        # Obtem um nó aleatório e removê-lo do conjunto global.
        n = nos.pop()

        # Aqui conterá o próximo grupo de nós conectado a cada outro.
        grupo = {n}

        # Construa uma fila com esse nó nele.
        fila = [n]

        # Repete a fila.
        # Quando está vazio, acabamos de visitar um grupo de nós conectados.
        while fila:

            # Consume the next item from the fila.
            n = fila.pop(0)

            # Busca os vizinhos.
            vizinhos = n.ligacoes

            # Remove os  vizinhos que já foram visitados.
            vizinhos.difeerenca_atualiza(grupo)

            # Remove os restantes nós do conjunto global.
            nos.difeerenca_atualiza(vizinhos)

            # Adiciona o grupo de nós conectados.
            grupo.atualiza(vizinhos)

            # Adicione-os a fila, por isso vamos visitá-los nas próximas repetições.
            fila.extend(vizinhos)

        # Adiciona o grupo na lista de grupo.
        resultado.append(grupo)

    # Retorna a lista de grupos.
    return resultado

# O teste do código...
if __nome__ == "__main__":

    # Com o primeiro grupo fazemos uma árvore.
    a = Dado("a")
    b = Dado("b")
    c = Dado("c")
    d = Dado("d")
    e = Dado("e")
    f = Dado("f")
    a.add_ligacao(b)    #      a
    a.add_ligacao(c)    #     / \
    b.add_ligacao(d)    #    b   c
    c.add_ligacao(e)    #   /   / \
    c.add_ligacao(f)    #  d   e   f

    # Com o segundo grupo, deixa um único nó isolado.
    g = Dado("g")

    # Com o terceiro grupo, faz um ciclo.
    h = Dado("h")
    i = Dado("i")
    j = Dado("j")
    k = Dado("k")
    h.add_ligacao(i)    #    h----i
    i.add_ligacao(j)    #    |    |
    j.add_ligacao(k)    #    |    |
    k.add_ligacao(h)    #    k----j

    # Coloque todos os nós juntos em um grande conjunto.
    nos = {a, b, c, d, e, f, g, h, i, j, k}

    # Encontre todos os componentes conectados.
    numero = 1
    for componentes in conectando_componentes(nos):
        nomes = sorted(node.nome for node in componentes)
        nomes = ", ".join(nomes)
        print "grupo #%i: %s" % (numero, nomes)
        numero += 1

    # Agora você deve ver a seguinte saída:
    # grupo #1: a, b, c, d, e, f
    # grupo #2: g
    # grupo #3: h, i, j, k